
# hash value = 153174948
idresourcestringsuriutils.rsutf16indexoutofrange='Character Index %d out '+
'of Range, Length = %d'


# hash value = 118055701
idresourcestringsuriutils.rsutf16invalidhighsurrogate='Character at Index'+
' %d is not a valid UTF-16 High Surrogate'


# hash value = 100068421
idresourcestringsuriutils.rsutf16invalidlowsurrogate='Character at Index '+
'%d is not a valid UTF-16 Low Surrogate'


# hash value = 57794629
idresourcestringsuriutils.rsutf16missinglowsurrogate='Missing a Low Surro'+
'gate in UTF-16 sequence'

