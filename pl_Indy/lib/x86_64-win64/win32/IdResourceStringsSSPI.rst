
# hash value = 42843516
idresourcestringssspi.rshttpsspisuccess='Successfull API call'


# hash value = 185236292
idresourcestringssspi.rshttpsspinotenoughmem='Not enough memory is availa'+
'ble to complete this request'


# hash value = 232235764
idresourcestringssspi.rshttpsspiinvalidhandle='The handle specified is in'+
'valid'


# hash value = 139843172
idresourcestringssspi.rshttpsspifuncnotsupported='The function requested '+
'is not supported'


# hash value = 185743269
idresourcestringssspi.rshttpsspiunknowntarget='The specified target is un'+
'known or unreachable'


# hash value = 202046164
idresourcestringssspi.rshttpsspiinternalerror='The Local Security Authori'+
'ty cannot be contacted'


# hash value = 185293236
idresourcestringssspi.rshttpsspisecpackagenotfound='The requested securit'+
'y package does not exist'


# hash value = 112107795
idresourcestringssspi.rshttpsspinotowner='The caller is not the owner of '+
'the desired credentials'


# hash value = 199769796
idresourcestringssspi.rshttpsspipackagecannotbeinstalled='The security pa'+
'ckage failed to initialize, and cannot be installed'


# hash value = 52400628
idresourcestringssspi.rshttpsspiinvalidtoken='The token supplied to the f'+
'unction is invalid'


# hash value = 227300756
idresourcestringssspi.rshttpsspicannotpack='The security package is not a'+
'ble to marshall the logon buffer, so the logon attempt has failed'


# hash value = 148942117
idresourcestringssspi.rshttpsspiqopnotsupported='The per-message Quality '+
'of Protection is not supported by the security package'


# hash value = 99666052
idresourcestringssspi.rshttpsspinoimpersonation='The security context doe'+
's not allow impersonation of the client'


# hash value = 233821284
idresourcestringssspi.rshttpsspilogindenied='The logon attempt failed'


# hash value = 220059300
idresourcestringssspi.rshttpsspiunknowncredentials='The credentials suppl'+
'ied to the package were not recognized'


# hash value = 217116437
idresourcestringssspi.rshttpsspinocredentials='No credentials are availab'+
'le in the security package'


# hash value = 177588324
idresourcestringssspi.rshttpsspimessagealtered='The message or signature '+
'supplied for verification has been altered'


# hash value = 97241173
idresourcestringssspi.rshttpsspioutofsequence='The message supplied for v'+
'erification is out of sequence'


# hash value = 92539278
idresourcestringssspi.rshttpsspinoauthauthority='No authority could be co'+
'ntacted for authentication.'


# hash value = 29293844
idresourcestringssspi.rshttpsspicontinueneeded='The function completed su'+
'ccessfully, but must be called again to complete the context'


# hash value = 252204436
idresourcestringssspi.rshttpsspicompleteneeded='The function completed su'+
'ccessfully, but CompleteToken must be called'


# hash value = 155916324
idresourcestringssspi.rshttpsspicompletecontinueneeded='The function comp'+
'leted successfully, but both CompleteToken and this function must be cal'+
'led to complete the context'


# hash value = 116005230
idresourcestringssspi.rshttpsspilocallogin='The logon was completed, but '+
'no network authority was available. The logon was made using locally kno'+
'wn information'


# hash value = 185293236
idresourcestringssspi.rshttpsspibadpackageid='The requested security pack'+
'age does not exist'


# hash value = 217280910
idresourcestringssspi.rshttpsspicontextexpired='The context has expired a'+
'nd can no longer be used.'


# hash value = 173562894
idresourcestringssspi.rshttpsspiincompletemessage='The supplied message i'+
's incomplete.  The signature was not verified.'


# hash value = 145941790
idresourcestringssspi.rshttpsspiincompletecredentialnotinit='The credenti'+
'als supplied were not complete, and could not be verified. The context c'+
'ould not be initialized.'


# hash value = 32532622
idresourcestringssspi.rshttpsspibuffertoosmall='The buffers supplied to a'+
' function was too small.'


# hash value = 182147070
idresourcestringssspi.rshttpsspiincompletecredentialsinit='The credential'+
's supplied were not complete, and could not be verified. Additional info'+
'rmation can be returned from the context.'


# hash value = 134029998
idresourcestringssspi.rshttpsspirengotiate='The context data must be rene'+
'gotiated with the peer.'


# hash value = 202745198
idresourcestringssspi.rshttpsspiwrongprincipal='The target principal name'+
' is incorrect.'


# hash value = 22775742
idresourcestringssspi.rshttpsspinolsacode='There is no LSA mode context a'+
'ssociated with this context.'


# hash value = 174529086
idresourcestringssspi.rshttpsspitimescew='The clocks on the client and se'+
'rver machines are skewed.'


# hash value = 67269630
idresourcestringssspi.rshttpsspiuntrustedroot='The certificate chain was '+
'issued by an untrusted authority.'


# hash value = 167477166
idresourcestringssspi.rshttpsspiillegalmessage='The message received was '+
'unexpected or badly formatted.'


# hash value = 151145598
idresourcestringssspi.rshttpsspicertunknown='An unknown error occurred wh'+
'ile processing the certificate.'


# hash value = 10124974
idresourcestringssspi.rshttpsspicertexpired='The received certificate has'+
' expired.'


# hash value = 234815614
idresourcestringssspi.rshttpsspiencryptionfailure='The specified data cou'+
'ld not be encrypted.'


# hash value = 234827646
idresourcestringssspi.rshttpsspidecryptionfailure='The specified data cou'+
'ld not be decrypted.'


# hash value = 102905246
idresourcestringssspi.rshttpsspialgorithmmismatch='The client and server '+
'cannot communicate, because they do not possess a common algorithm.'


# hash value = 123656382
idresourcestringssspi.rshttpsspisecurityqosfailure='The security context '+
'could not be established due to a failure in the requested quality of se'+
'rvice (e.g. mutual authentication or delegation).'


# hash value = 46297006
idresourcestringssspi.rshttpsspisecctxwasdelbeforeupdated='A security con'+
'text was deleted before the context was completed. This is considered a '+
'logon failure.'


# hash value = 133609038
idresourcestringssspi.rshttpsspiclientnotgtreply='The client is trying to'+
' negotiate a context and the server requires user-to-user but didn'#39't'+
' send a TGT reply.'


# hash value = 14728734
idresourcestringssspi.rshttpsspilocalnoipaddr='Unable to accomplish the r'+
'equested task because the local machine does not have any IP addresses.'


# hash value = 71956222
idresourcestringssspi.rshttpsspiwrongcredhandle='The supplied credential '+
'handle does not match the credential associated with the security contex'+
't.'


# hash value = 53693454
idresourcestringssspi.rshttpsspicryptosysinvalid='The crypto system or ch'+
'ecksum function is invalid because a required function is unavailable.'


# hash value = 63759406
idresourcestringssspi.rshttpsspimaxticketref='The number of maximum ticke'+
't referrals has been exceeded.'


# hash value = 190944718
idresourcestringssspi.rshttpsspimustbekdc='The local machine must be a Ke'+
'rberos KDC (domain controller) and it is not.'


# hash value = 266654190
idresourcestringssspi.rshttpsspistrongcryptonotsupported='The other end o'+
'f the security negotiation is requires strong crypto but it is not suppo'+
'rted on the local machine.'


# hash value = 184953310
idresourcestringssspi.rshttpsspikdcreplytoomanyprincipals='The KDC reply '+
'contained more than one principal name.'


# hash value = 220602766
idresourcestringssspi.rshttpsspinopadata='Expected to find PA data for a '+
'hint of what etype to use, but it was not found.'


# hash value = 119391486
idresourcestringssspi.rshttpsspipkinitnamemismatch='The client certificat'+
'e does not contain a valid UPN, or does not match the client name in the'+
' logon request. Please contact your administrator.'


# hash value = 241989054
idresourcestringssspi.rshttpsspismartcardlogonreq='Smartcard logon is req'+
'uired and was not used.'


# hash value = 157151150
idresourcestringssspi.rshttpsspisysshutdowninprog='A system shutdown is i'+
'n progress.'


# hash value = 66195166
idresourcestringssspi.rshttpsspikdcinvalidrequest='An invalid request was'+
' sent to the KDC.'


# hash value = 231915390
idresourcestringssspi.rshttpsspikdcunabletorefer='The KDC was unable to g'+
'enerate a referral for the service requested.'


# hash value = 109608926
idresourcestringssspi.rshttpsspikdcetypeunknown='The encryption type requ'+
'ested is not supported by the KDC.'


# hash value = 217926510
idresourcestringssspi.rshttpsspiunsuppreauth='An unsupported preauthentic'+
'ation mechanism was presented to the Kerberos package.'


# hash value = 192507806
idresourcestringssspi.rshttpsspideligationreq='The requested operation ca'+
'nnot be completed. The computer must be trusted for delegation and the c'+
'urrent user account must be configured to allow delegation.'


# hash value = 154356670
idresourcestringssspi.rshttpsspibadbindings='Client'#39's supplied SSPI c'+
'hannel bindings were incorrect.'


# hash value = 138495102
idresourcestringssspi.rshttpsspimultipleaccounts='The received certificat'+
'e was mapped to multiple accounts.'


# hash value = 18011497
idresourcestringssspi.rshttpsspinokerbkey='SEC_E_NO_KERB_KEY'


# hash value = 85494766
idresourcestringssspi.rshttpsspicertwrongusage='The certificate is not va'+
'lid for the requested usage.'


# hash value = 130387870
idresourcestringssspi.rshttpsspidowngradedetected='The system detected a '+
'possible attempt to compromise security. Please ensure that you can cont'+
'act the server that authenticated you.'


# hash value = 117564478
idresourcestringssspi.rshttpsspismartcardcertrevoked='The smartcard certi'+
'ficate used for authentication has been revoked. Please contact your sys'+
'tem administrator. There may be additional information in the event log.'+


# hash value = 62467214
idresourcestringssspi.rshttpsspiissuingcauntrusted='An untrusted certific'+
'ate authority was detected While processing the smartcard certificate us'+
'ed for authentication. Please contact your system administrator.'


# hash value = 265299006
idresourcestringssspi.rshttpsspirevocationoffline='The revocation status '+
'of the smartcard certificate used for authentication could not be determ'+
'ined. Please contact your system administrator.'


# hash value = 128975742
idresourcestringssspi.rshttpsspipkinitclientfailure='The smartcard certif'+
'icate used for authentication was not trusted. Please contact your syste'+
'm administrator.'


# hash value = 125860590
idresourcestringssspi.rshttpsspismartcardexpired='The smartcard certifica'+
'te used for authentication has expired. Please contact your system admin'+
'istrator.'


# hash value = 139281118
idresourcestringssspi.rshttpsspinos4uprotsupport='The Kerberos subsystem '+
'encountered an error. A service for user protocol request was made again'+
'st a domain controller which does not support service for user.'


# hash value = 96240670
idresourcestringssspi.rshttpsspicrossrealmdeligationfailure='An attempt w'+
'as made by this server to make a Kerberos constrained delegation request'+
' for a target outside of the server'#39's realm. This is not supported, '+
'and indicates a misconfiguration on this server'#39's allowed to delegat'+
'e to list. Please contact your administrator.'


# hash value = 239741950
idresourcestringssspi.rshttpsspirevocationofflinekdc='The revocation stat'+
'us of the domain controller certificate used for smartcard authenticatio'+
'n could not be determined. There is additional information in the system'+
' event log. Please contact your system administrator.'


# hash value = 2964878
idresourcestringssspi.rshttpsspicauntrustedkdc='An untrusted certificate '+
'authority was detected while processing the domain controller certificat'+
'e used for authentication. There is additional information in the system'+
' event log. Please contact your system administrator.'


# hash value = 262338014
idresourcestringssspi.rshttpsspikdccertexpired='The domain controller cer'+
'tificate used for smartcard logon has expired. Please contact your syste'+
'm administrator with the contents of your system event log.'


# hash value = 67737870
idresourcestringssspi.rshttpsspikdccertrevoked='The domain controller cer'+
'tificate used for smartcard logon has been revoked. Please contact your '+
'system administrator with the contents of your system event log.'


# hash value = 5505134
idresourcestringssspi.rshttpsspisignatureneeded='A signature operation mu'+
'st be performed before the user can authenticate.'


# hash value = 193792910
idresourcestringssspi.rshttpsspiinvalidparameter='One or more of the para'+
'meters passed to the function was invalid.'


# hash value = 227615438
idresourcestringssspi.rshttpsspideligationpolicy='Client policy does not '+
'allow credential delegation to target server.'


# hash value = 219204078
idresourcestringssspi.rshttpsspipolicyntlmonly='Client policy does not al'+
'low credential delegation to target server with NLTM only authentication'+
'.'


# hash value = 200067982
idresourcestringssspi.rshttpsspinorenegotiation='The recipient rejected t'+
'he renegotiation request.'


# hash value = 22534078
idresourcestringssspi.rshttpsspinocontext='The required security context '+
'does not exist.'


# hash value = 41395854
idresourcestringssspi.rshttpsspipku2ucertfailure='The PKU2U protocol enco'+
'untered an error while attempting to utilize the associated certificates'+
'.'


# hash value = 90760798
idresourcestringssspi.rshttpsspimutualauthfailed='The identity of the ser'+
'ver computer could not be verified.'


# hash value = 205443058
idresourcestringssspi.rshttpsspiunknwonerror='Unknown error'


# hash value = 215332947
idresourcestringssspi.rshttpsspierrormsg='SSPI %s returns error #%d(0x%x)'+
': %s'


# hash value = 108090217
idresourcestringssspi.rshttpsspiinterfaceinitfailed='SSPI interface has f'+
'ailed to initialise properly'


# hash value = 28769188
idresourcestringssspi.rshttpsspinopkginfospecified='No PSecPkgInfo specif'+
'ied'


# hash value = 164209476
idresourcestringssspi.rshttpsspinocredentialhandle='No credential handle '+
'acquired'


# hash value = 49511236
idresourcestringssspi.rshttpsspicannotchangecredentials='Can not change c'+
'redentials after handle aquired. Use Release first'


# hash value = 185409205
idresourcestringssspi.rshttpsspiunknwoncredentialuse='Unknown credentials'+
' use'


# hash value = 137624580
idresourcestringssspi.rshttpsspidoauquirecredentialhandle='Do AcquireCred'+
'entialsHandle first'


# hash value = 26500788
idresourcestringssspi.rshttpsspicompletetokennotsupported='CompleteAuthTo'+
'ken is not supported'

