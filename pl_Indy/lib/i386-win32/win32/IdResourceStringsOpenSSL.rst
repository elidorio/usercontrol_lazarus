
# hash value = 130365950
idresourcestringsopenssl.rsossfailedtoload='Failed to load %s.'


# hash value = 668702
idresourcestringsopenssl.rsosslmodenotset='Mode has not been set.'


# hash value = 4851454
idresourcestringsopenssl.rsosslcouldnotloadssllibrary='Could not load SSL'+
' library.'


# hash value = 23937138
idresourcestringsopenssl.rsosslstatusstring='SSL status: "%s"'


# hash value = 206554350
idresourcestringsopenssl.rsosslconnectiondropped='SSL connection has drop'+
'ped.'


# hash value = 181744078
idresourcestringsopenssl.rsosslcertificatelookup='SSL certificate request'+
' error.'


# hash value = 183180814
idresourcestringsopenssl.rsosslinternal='SSL library internal error.'


# hash value = 88616532
idresourcestringsopenssl.rsosslalert='%s Alert'


# hash value = 169060292
idresourcestringsopenssl.rsosslreadalert='%s Read Alert'


# hash value = 35307252
idresourcestringsopenssl.rsosslwritealert='%s Write Alert'


# hash value = 119852448
idresourcestringsopenssl.rsosslacceptloop='Accept Loop'


# hash value = 37029138
idresourcestringsopenssl.rsosslaccepterror='Accept Error'


# hash value = 56509844
idresourcestringsopenssl.rsosslacceptfailed='Accept Failed'


# hash value = 119756996
idresourcestringsopenssl.rsosslacceptexit='Accept Exit'


# hash value = 247694032
idresourcestringsopenssl.rsosslconnectloop='Connect Loop'


# hash value = 204067970
idresourcestringsopenssl.rsosslconnecterror='Connect Error'


# hash value = 43211892
idresourcestringsopenssl.rsosslconnectfailed='Connect Failed'


# hash value = 247634868
idresourcestringsopenssl.rsosslconnectexit='Connect Exit'


# hash value = 65208836
idresourcestringsopenssl.rsosslhandshakestart='Handshake Start'


# hash value = 155144421
idresourcestringsopenssl.rsosslhandshakedone='Handshake Done'

